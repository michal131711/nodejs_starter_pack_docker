FROM node:18

RUN npm cache clean -f

RUN npm install -g n

RUN n stable

RUN npm install --global express

RUN npm install --global sequelize

RUN npm install --global ts-node

RUN npm install --global nodemon

CMD npm install --global ts-node

WORKDIR ./starter_nodejs

ADD starter_nodejs /starter_nodejs

RUN npm install

RUN npm rebuild bcrypt --build-from-source

EXPOSE 3000
